package com.example.gm.materiladesign;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;



import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
     private ArrayList<Jobs> jobsList =new ArrayList<Jobs>();
     private CustomAdapter adapter ;
   //  private  Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //MainActivity activity = new MainActivity();
        //StatusBarUtil.setTransparent(activity);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        toolbar.setTitle("Jobs");
        setSupportActionBar(toolbar);
        jobsList.add(new Jobs(R.drawable.rakul, "Austin", "She is a heroin", R.drawable.star, "Film", "As this is just a custom ImageView and not a custom Drawable or a combination of both, it can be used with all kinds of drawables, i.e. a PicassoDrawable from Picasso or other non-standard drawables.",
                "videography", "catalog", "visualEmphasis", "modelRegonization"));

        jobsList.add(new Jobs(R.drawable.rakul, "Austin", "She is a heroin", R.drawable.star, "Film", "As this is just a custom ImageView and not a custom Drawable or a combination of both, it can be used with all kinds of drawables, i.e. a PicassoDrawable from Picasso or other non-standard drawables.",
                "videography", "catalog", "visualEmphasis", "modelRegonization"));
        jobsList.add(new Jobs(R.drawable.rakul, "Austin", "She is a heroin", R.drawable.star, "Film", "As this is just a custom ImageView and not a custom Drawable or a combination of both, it can be used with all kinds of drawables, i.e. a PicassoDrawable from Picasso or other non-standard drawables.",
                "videography", "catalog", "visualEmphasis", "modelRegonization"));
        jobsList.add(new Jobs(R.drawable.rakul, "Austin", "She is a heroin", R.drawable.star, "Film", "As this is just a custom ImageView and not a custom Drawable or a combination of both, it can be used with all kinds of drawables, i.e. a PicassoDrawable from Picasso or other non-standard drawables.",
                "videography", "catalog", "visualEmphasis", "modelRegonization"));


        adapter = new CustomAdapter(jobsList, this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


    }
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menuitems, menu);


    /*CardView cardView = findViewById(R.id.austinCard_view);

        cardView.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                Toast.makeText(MainActivity.this, "CardView Clicked", Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(getApplicationContext(),Austin.class);
                 startActivity(intent);
                //call();

            }
        });*/


        return true;
    }

    /*public  void call() {
        Intent intent = new Intent(getApplicationContext(), Austin.class);
        startActivity(intent);

    }*/


}
