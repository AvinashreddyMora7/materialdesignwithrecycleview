package com.example.gm.materiladesign;

import android.widget.ImageView;
import android.widget.TextView;

public class Jobs {
    int rakul ;
    String austin;
    String heroin;
    int star;
    String film;
    String filmText;
    String videoGraphy;
    String catalog;
    String visualEmphasis;
    String model;

    public Jobs(int rakul, String austin, String heroin, int star, String film, String filmText, String videoGraphy, String catalog, String visualEmphasis, String model) {
        this.rakul = rakul;
        this.austin = austin;
        this.heroin = heroin;
        this.star = star;
        this.film = film;
        this.filmText = filmText;
        this.videoGraphy = videoGraphy;
        this.catalog = catalog;
        this.visualEmphasis = visualEmphasis;
        this.model = model;
    }

    public int getRakul() {
        return rakul;
    }

    public void setRakul(int rakul) {
        this.rakul = rakul;
    }

    public String getAustin() {
        return austin;
    }

    public void setAustin(String austin) {
        this.austin = austin;
    }

    public String getHeroin() {
        return heroin;
    }

    public void setHeroin(String heroin) {
        this.heroin = heroin;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getFilm() {
        return film;
    }

    public void setFilm(String film) {
        this.film = film;
    }

    public String getFilmText() {
        return filmText;
    }

    public void setFilmText(String filmText) {
        this.filmText = filmText;
    }

    public String getVideoGraphy() {
        return videoGraphy;
    }

    public void setVideoGraphy(String videoGraphy) {
        this.videoGraphy = videoGraphy;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getVisualEmphasis() {
        return visualEmphasis;
    }

    public void setVisualEmphasis(String visualEmphasis) {
        this.visualEmphasis = visualEmphasis;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
