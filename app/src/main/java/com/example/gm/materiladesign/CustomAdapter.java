package com.example.gm.materiladesign;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    Context context;
    private ArrayList<Jobs> jobsList;


   public  CustomAdapter(ArrayList<Jobs> jobsList, Context context){
       this.jobsList = jobsList;
       this.context = context;
   }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView rakul;
        TextView austin;
        TextView heroin ;
        ImageView star ;
        TableRow tableRow;
        TextView film;
        TextView filmText;
        TextView videoGraphy;
        TextView catalog;
        TextView visual;
        TextView model;
        public MyViewHolder(final View itemView) {
            super(itemView);


            rakul = itemView.findViewById(R.id.rakul);
            austin = (TextView) itemView.findViewById(R.id.austin);
            heroin = (TextView) itemView.findViewById(R.id.heroin);
            star = (ImageView) itemView.findViewById(R.id.star);
            tableRow = (TableRow) itemView.findViewById(R.id.tableRow);
            film= (TextView) itemView.findViewById(R.id.film);
            filmText = (TextView) itemView.findViewById(R.id.filmtext);
            videoGraphy= (TextView) itemView.findViewById(R.id.videography);
            catalog= (TextView) itemView.findViewById(R.id.catalog);
            visual= (TextView) itemView.findViewById(R.id.visualemphasis);
            model= (TextView) itemView.findViewById(R.id.model);


            View view = itemView;
            view.setOnClickListener(new View.OnClickListener() {


                @Override public void onClick(View v) {
                    Toast.makeText(context, "cardViewClicked",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(context, Austin.class);
                    context.startActivity(intent);
                }
            });

        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

         View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listitems, viewGroup, false);

         MyViewHolder myViewHolder = new MyViewHolder(view);



        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int i) {
        Jobs jobs = jobsList.get(i);
         viewHolder.rakul.setImageResource(jobs.getRakul());
        viewHolder.austin.setText(jobs.getAustin());
        viewHolder.heroin.setText(jobs.getHeroin());
        viewHolder.star.setImageResource(jobs.getStar());
        viewHolder.film.setText(jobs.getFilm());
        viewHolder.filmText.setText(jobs.getFilmText());
        viewHolder.videoGraphy.setText(jobs.getVideoGraphy());
        viewHolder.catalog.setText(jobs.getCatalog());
        viewHolder.visual.setText(jobs.getVisualEmphasis());
        viewHolder.model.setText(jobs.getModel());
    }


    @Override
    public int getItemCount() {
        return jobsList.size();
    }



}
